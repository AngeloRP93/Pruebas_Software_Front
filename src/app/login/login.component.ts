import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { ImportacionesService } from '../app-modules/services/importacionesSyR.service';
import { Http } from '@angular/http';
import { Router } from '@angular/router';

@Component({
  selector: 'login',
  styleUrls: ['./login.style.scss'],
  templateUrl: './login.template.html',
  encapsulation: ViewEncapsulation.None,
  host: {
    class: 'login-page app'
  }
})
export class Login implements OnInit {
  anio: number = new Date().getFullYear();
  constructor(
    private router: Router,
    private http: Http,
    private loginSrv: ImportacionesService) {
  }

  ngOnInit() {
    // Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    // Add 'implements OnInit' to the class.
  }

  login(email, password) {
    console.log('Email:' + email);
    console.log('Password:' + password);

    this.resetLoginRouteService();
    this.loginSrv.post(
      {
        email:email,
        password:password
      }
    ).subscribe(
      loginResponse => {
        console.log('LOgin JSon');
        console.log(loginResponse);
        console.log(loginResponse.data.users[0].name);

        console.log('++++++++++++++++++');
        window.localStorage.setItem('userName',loginResponse.data.users[0].name);
        this.router.navigate(['/app/productos']);
      },
      error => {
        console.log('Error');
        console.log(error);
      }
      );
    console.log();


  }

  private resetLoginRouteService() {
    this.loginSrv = new ImportacionesService(this.http);
    this.loginSrv.webAddress.addUrl('login');
  }
}
