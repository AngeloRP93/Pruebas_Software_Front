
import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'mmm',
  template:
  `
  <div>
    <selector-table [headers] ="headers" [list] ="list"></selector-table>
  </div>`
})

export class LlamadorComponent implements OnInit {
  headers = [
    'Name', 'Apellido', 'Dinero'
  ];
  list = [
    ['Pepo', 'Steam', '20.10'],
    ['Larry', 'Capija', '24'],
    ['Analis','Melorto', '2111']
    ];
  constructor() {
  }

  ngOnInit() { }
}