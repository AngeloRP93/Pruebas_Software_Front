import { NgModule } from '@angular/core';

import { TableComponent } from './table.component';

@NgModule({
  imports: [],
  exports: [],
  declarations: [TableComponent],
  providers: [],
})
export class TableModule { }
