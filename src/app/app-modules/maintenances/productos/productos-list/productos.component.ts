import { Component, OnInit } from '@angular/core';

import { Http } from '@angular/http';
import { ImportacionesService } from '../../../services/importacionesSyR.service';
import { LoaderObject } from '../../../super/objeto-loader';
import { Column, TipoEntrada, HttpOperations, InputSelector } from '../../../interfaces/interfaces';
import { TableData } from '../../../super/table-data';
@Component({
  selector: 'app-productos',
  templateUrl: 'productos.component.html',
  styleUrls: ['./productos.component.scss']
})

export class ProductosComponent extends TableData implements OnInit {
  constructor() {
    super('Productos');
    this.itemsPerPage = 10;
    this.titulo = 'Producto';
    this.url = 'productos';
    this.modalAdd = {
      titulo: 'Agregar Producto',
      url: 'register',
      type: HttpOperations.POST,
      formResponse: [null, null, null, null, null, null],
      formResponseJSON: {
        pro_COD: '',
        pro_nombre: '',
        pro_cantidad: '',
        pro_cantidad_desc: '',
        pro_precio: '',
        pro_precio_desc: ''
      },
      formInput: [
        {
          column: { title: 'Cod. Producto', name: 'pro_COD', type: 'text' },
          type: InputSelector.INPUT
        },
        { column: { title: 'Nombre', name: 'pro_nombre', type: 'text' }, type: InputSelector.INPUT },
        { column: { title: 'Cantidad', name: 'pro_cantidad', type: 'number' }, type: InputSelector.INPUT },
        {
          column: {
            title: 'Cantidad Desc', name: 'pro_cantidad_desc',
            type: 'number'
          }, type: InputSelector.INPUT
        },
        { column: { title: 'Precio', name: 'pro_precio', type: 'number' }, type: InputSelector.INPUT },
        { column: { title: 'Descuento %', name: 'pro_precio_desc', type: 'number' }, type: InputSelector.INPUT },
      ]
    };
    this.modalEdit = {
      titulo: 'Editar Producto',
      url: 'edit',
      type: HttpOperations.PUT,
      formResponse: [null, null, null, null, null, null],
      formResponseJSON: {
        pro_COD: '',
        pro_nombre: '',
        pro_cantidad: '',
        pro_cantidad_desc: '',
        pro_precio: '',
        pro_precio_desc: ''
      },
      formInput: [
        { column: { title: 'Codigo Pro', name: 'pro_COD' }, type: InputSelector.INPUT },
        { column: { title: 'Nombre', name: 'pro_nombre' }, type: InputSelector.INPUT },
        { column: { title: 'Cantidad', name: 'pro_cantidad' }, type: InputSelector.INPUT },
        { column: { title: 'Cantidad Desc', name: 'pro_cantidad_desc' }, type: InputSelector.INPUT },
        { column: { title: 'Precio', name: 'pro_precio' }, type: InputSelector.INPUT },
        { column: { title: 'Descuento %', name: 'pro_precio_desc' }, type: InputSelector.INPUT },
      ]
    };
    this.buttons = [
      { img: 'glyphicon glyphicon-pencil', name: 'edit', desc: 'Editar Producto' }
    ];
    this.addButton = {
      existe: true,
      desc: 'Insertar Producto',
      img: 'fa fa-plus'
    };
    this.columns = [
      { title: 'Codigo de Producto', name: 'pro_COD', type: 'string', sort: 'asc' },
      { title: 'Nombre', name: 'pro_nombre', type: 'string' },
      { title: 'Cantidad', name: 'pro_cantidad', type: 'number' },
      { title: 'Cantidad Desc', name: 'pro_cantidad_desc', type: 'number' },
      { title: 'Precio', name: 'pro_precio', type: 'float' },
      { title: 'Descuento', name: 'pro_precio_desc', type: 'float' }
    ];
    this.config = {
      paging: true,
      sorting: this.columns,
      filtering: { filterString: '', columnName: 'pro_COD' }
    };
  }

  ngOnInit() {

  }
}
