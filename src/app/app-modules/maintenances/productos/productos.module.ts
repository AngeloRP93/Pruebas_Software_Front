import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { ProductosComponent } from './productos-list/productos.component';
import { DescuentosComponent } from './descuentos/descuentos.component';
import { SharedModule } from '../../shared/shared.module';
import { RouterModule } from '@angular/router';
import { ImportacionesService } from '../../services/importacionesSyR.service';
export const routes = [
  { path: '', component: ProductosComponent, pathMatch: 'full' },
  { path: 'descuentos/:id', component: DescuentosComponent }
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(routes),
  ],
  exports: [],
  declarations: [
    ProductosComponent,
    DescuentosComponent
  ],

  providers: [ImportacionesService]
})
export class ProductosModule {
  static routes = routes;
}
