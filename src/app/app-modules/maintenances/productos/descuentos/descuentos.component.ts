import { Component, OnInit, ViewContainerRef, ViewEncapsulation } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Router, ActivatedRoute } from '@angular/router';
import { LoaderObject } from '../../../super/objeto-loader';
import { Http } from '@angular/http';
import { ImportacionesService } from '../../../services/importacionesSyR.service';
declare let jQuery: any;
@Component({
  selector: '[descuentos]',
  templateUrl: 'descuentos.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./descuentos.component.scss'],
  providers: []
})

export class DescuentosComponent extends LoaderObject implements OnInit {
  data: any;
closeResult: string;
  constructor(
    private modalService: NgbModal,
    private http: Http,
    private descuentosSrv: ImportacionesService,
    private route: ActivatedRoute,
    private router: Router,
  ) {
    super('Descuentos');
  }

  ngOnInit() {
    this.route.params.subscribe(
      param => {
        console.log(param['id']);
        if (param['id'] === 'undefined') {
          this.router.navigate(['/productos']);
        } else {
          this.open('Tazdingo');

          // this.resetRouteService();
          this.descuentosSrv = new ImportacionesService(this.http);
          this.descuentosSrv.webAddress.addUrl('descuentos');
          this.descuentosSrv.webAddress.addUrl(param['id']);
          this.descuentosSrv.get().subscribe(
            descuento => {
              console.log('Se consiguio');
            },
            error => {
              console.log('Error');
            }
          );
        }
      }
    );
  }

  ngAfterViewInit() {
    // Called after ngAfterContentInit when the component's
    // view has been initialized. Applies to components only.
    // Add 'implements AfterViewInit' to the class.
  }

  open(content) {
    this.modalService.open(content).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  private resetRouteService() {
    this.descuentosSrv = new ImportacionesService(this.http);
    this.descuentosSrv.webAddress.addUrl('descuentos');
  }
}
