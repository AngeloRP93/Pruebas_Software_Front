import { Component, OnInit } from '@angular/core';
import { ImportacionesService } from '../../../services/importacionesSyR.service';
import { HttpOperations, InputSelector } from '../../../interfaces/interfaces';
import { Http } from '@angular/http';
import { TableData } from '../../../super/table-data';

@Component({
  selector: 'app-admin-list',
  templateUrl: './users-list.component.html'

})

export class UsersComponent extends TableData implements OnInit {
  constructor() {
    super('Usuarios');
    this.titulo = 'Usuario';
    this.url = 'users';
    this.modalAdd = {
      titulo: 'Registrar Usuario',
      url: 'register',
      type: HttpOperations.POST,
      formResponse: [null, null, null],
      formResponseJSON: {
        user_nombre: '',
        user_email: '',
        user_rol: ''
      },
      formInput: [
        { column: { title: 'Nombre', name: 'user_nombre' }, type: InputSelector.INPUT },
        { column: { title: 'Email', name: 'user_email' }, type: InputSelector.INPUT },
        { column: { title: 'Rol', name: 'user_rol' }, type: InputSelector.SELECT, url: 'roles' },
      ]
    };
    this.modalEdit = {
      titulo: 'Editar Producto',
      url: 'edit',
      type: HttpOperations.PUT,
      formResponse: [null, null, null],
      formResponseJSON: {
        user_nombre: '',
        user_email: '',
        user_rol: ''
      },
      formInput: [
        { column: { title: 'Nombre', name: 'user_nombre' }, type: InputSelector.INPUT },
        { column: { title: 'Email', name: 'user_email' }, type: InputSelector.INPUT },
        { column: { title: 'Rol', name: 'user_rol' }, type: InputSelector.SELECT, url: 'roles' },
      ]
    };
    this.buttons = [
      { img: 'glyphicon glyphicon-pencil', name: 'edit', desc: 'Editar Usuario' }
    ];
    this.addButton = {
      existe: true,
      desc: 'Registrar Nuevo Usuario',
      img: 'fa fa-plus'
    };
    this.columns = [
      { title: 'Nombre', name: 'user_nombre', sort: 'asc' },
      { title: 'Correo', name: 'user_email' },
      { title: 'Rol', name: 'user_rol' }
    ];
    this.config = {
      paging: true,
      sorting: this.columns,
      filtering: { filterString: '', columnName: 'user_nombre' }
    };
  }

  ngOnInit() { }
}
