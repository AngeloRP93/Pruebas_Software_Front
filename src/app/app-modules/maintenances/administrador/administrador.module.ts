import { NgModule } from '@angular/core';
import { UsersComponent } from './users-list/users-list.component';
import { SharedModule } from '../../shared/shared.module';
import { RouterModule } from '@angular/router';

export const routes = [
  {path: '', redirectTo: 'admin', pathMatch: 'full'},
  {path: 'admin', component: UsersComponent}
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(routes),
  ],
  exports: [],
  declarations: [UsersComponent],
  providers: [],
})
export class AdministradorModule { }
