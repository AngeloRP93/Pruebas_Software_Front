import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TableComponent } from './table-list/table.component';
import { NameComponent } from './children.component';
import { LlamadorComponent } from './llamador.component';
import { Llamador2Component } from './llamador2.component';
import { LlamadorBotonesComponent } from './llamadorBotones.component';
export const routes = [
  { path: '', component: LlamadorComponent, pathMatch: 'full' },
  { path: 'mascotas', component: Llamador2Component, pathMatch: 'full' },
  { path: 'botones', component: LlamadorBotonesComponent, pathMatch: 'full' }
];
@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [],
  declarations: [TableComponent, NameComponent,
    LlamadorComponent, Llamador2Component, LlamadorBotonesComponent],
  providers: [],
})
export class PrincipalModule { static routes = routes; }
