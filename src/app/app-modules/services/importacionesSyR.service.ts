import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { EndPointService } from './endpoint.service';
import { config_server } from '../config/config';

@Injectable()
export class ImportacionesService extends EndPointService {
  constructor(public http: Http) {
    super(http, 'http://localhost:8000/api', config_server.headers);
  }

}
