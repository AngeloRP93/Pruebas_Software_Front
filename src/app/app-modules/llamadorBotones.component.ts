
import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'mmm',
  template:
  `
  <div>
    <selector-table [headers] ="headers" [list] ="list" [botones] ="botones"></selector-table>
  </div>`
})

export class LlamadorBotonesComponent implements OnInit {
  headers = [
    'Name', 'Dueño', 'Anios', 'Vidas', 'Options'
  ];
  html: any;
  list: any[][];
  botones: any[];
  constructor() {
  }

  ngOnInit() {
    this.list = [
      ['Michifus', 'pepo', '7', '4'],
      ['Larry', 'larry', '10', '6'],
      ['asss', 'sss', '5', '3'],
      ['sss', 'ssss', '4', '3']
    ];
    this.botones = [
      'activar', 'editar'
    ];
  }
}