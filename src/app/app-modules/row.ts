export class Row {
  private name: string;
  private data: Array<any>;

  constructor($data: any[], $name: string) {
    this.data = $data;
    this.name = $name;
  }

  public get $name() {
    return this.name;
  }

  public get $data(): any[] {
    return this.data;
  }

  public set $data(value: any[]) {
    this.data = value;
  }

  public addData(value: any) {
    // console.log('Name:' + this.name);
    // console.log('Anterior Tamanio:' + this.data.length);
    // console.log('Nuevo Tamanio:' + this.data.push(value));
  }

  public toString(){
    this.data.forEach(element => {
      console.log(element);
    });
  }

}
