import { LoaderObject } from './objeto-loader';
import { ModalData } from '../interfaces/interfaces';
export class TableData extends LoaderObject {
  modalAdd: ModalData;
  modalEdit: ModalData;
  addButton: any;
  // Tabla
  titulo: string = '';
  config: any;
  url: string;
  data: Array<any> = [];
  rows: Array<any> = [];
  columns: Array<any> = [];
  buttons: Array<any> = [];
  //Paginacion
  page: number = 1;
  length: number = 0;
  itemsPerPage: number = 10;
  maxSize: number = 5;
  numPages: number = 1;
}
