
import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'mmm',
  template:
  `
  <div>
    <selector-table [headers] ="headers" [list] ="list"></selector-table>
  </div>`
})

export class Llamador2Component implements OnInit {
  headers = [
    'Name', 'Dueño', 'Anios', 'Vidas'
  ];
  list = [
    ['Michifus', 'pepo', '7', '4'],
    ['Larry', 'larry', '10', '6'],
    ['asss', 'sss', '5', '3'],
    ['sss', 'ssss', '4', '3']
  ];
  constructor() {
  }

  ngOnInit() { }
}