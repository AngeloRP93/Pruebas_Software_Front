export enum HttpOperations {
  GET = 0,
  POST = 1,
  PUT = 2,
  PATCH = 3
}

export enum InputSelector {
  INPUT = 0,
  SELECT = 1,
  RADIO_BUTTON = 2
}
export enum TipoEntrada {
  String = 0,
  Entero = 1,
  Decimal = 2
}

export interface Column {
  title: string;
  name?: string;
  sort?: any;
  type?: string;
}

export interface FormElement {
  column: Column;
  type: InputSelector | InputSelector.INPUT;
  url?: string;
  clase?: string;

}

export interface ModalData {
  titulo: string;
  url: string;
  type: HttpOperations;
  formResponse: any;
  formResponseJSON: any;
  formInput: Array<FormElement>;
}

export interface Filtering {
  filterString: string;
  columnName: string;
}

export interface ConfigTable {
  paging: boolean;
  sorting: Array<Column>;
  filtering?: Filtering;
}

export interface HeaderDatos {
  hclass: string;
  bclass: string;
  name: string;
  value: any;
  option: number;
}

export interface Button {
  class: string;
  info: string;
  direccion: string;
  img: string;
  type: number;
}
