import { Row } from './row';
export class Table {
  private headers: string[];
  private rows: Array<Row>;

  constructor($headers: string[], $rows: Row[]) {
    this.headers = $headers;
    this.rows = $rows;
  }

  public get $rows(): Row[] {
    return this.rows;
  }

  public get $headers(): string[] {
    return this.headers;
  }

  public addRowToLast(row: Row) {
    this.rows.push(row);
  }

  public addRowToFirst(row: Row) {
    this.rows.unshift(row);
  }

  public addRow(row: Row, indice: number) {
    this.rows.fill(row, indice);
  }

  public removeLastRow() {
    console.log('Last Row Removida with elements');
    console.log(this.rows.pop());
  }

  public removeFirstRow() {
    console.log('First Row removida with elements')
    console.log(this.rows.shift().toString());
  }

  public removeRow(row: Row) {
    console.log('Row Removida with elements');
    const indice = this.rows.indexOf(row);
    console.log(this.rows.splice(indice, 1));
  }

  public removeRowByIndex(indice: number) {
    console.log('Row Removida with elements');
    console.log(this.rows.splice(indice, 1));
  }

  public mostrarItems() {
    let lenght = 1;
    this.rows.forEach(element => {
      console.log('Row:' + lenght);
      console.log(element.toString());
    });
  }

}