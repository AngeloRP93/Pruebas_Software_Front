import {
  NgbModalOptions,
  NgbModalRef,
  NgbModal,
  ModalDismissReasons
} from '@ng-bootstrap/ng-bootstrap';
import { FormElement, ModalData, HttpOperations } from '../../interfaces/interfaces';
import { ImportacionesService } from '../../services/importacionesSyR.service';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Input, Output, EventEmitter } from '@angular/core';
export class Modal {
  @Output() cerrarModal: EventEmitter<boolean>;
  @Output() actualizarTabla: EventEmitter<boolean>;
  @Input() content: any;
  @Input() modalData: ModalData;
  @Input() formAnterior: any;
  closeResult: string;
  select2Options: any = {
    theme: 'bootstrap'
  };
  private modalResponse = '';
  private modalOption: NgbModalOptions = {};
  private modal: NgbModalRef;

  constructor(
    public http: Http,
    public modalService: NgbModal,
    public operationService: ImportacionesService) {
    this.cerrarModal = new EventEmitter<any>();
    this.actualizarTabla = new EventEmitter<any>();
    this.modalOption.backdrop = 'static';
    this.modalOption.keyboard = false;
  }

  setModalData(modalData: ModalData) {
    this.modalData = modalData;
    this.formAnterior = this.modalData.formResponse;
  }

  open(modal, content?) {
    if (content) {
      console.log('Content:' + content);
      console.log('Content:' + JSON.stringify(content));
      for (let index = 0; index < this.modalData.formInput.length; index++) {
        this.modalData.formResponse[index] = content[this.modalData.formInput[index].column.name];
      }
    }
    console.log('Response Anterior:' + JSON.stringify(this.formAnterior));
    console.log('Modal Response:' + JSON.stringify(this.modalData.formResponse));
    console.log('Modal Input:' + JSON.stringify(this.modalData.formInput));
    //this.modalData.formResponse = this.formAnterior;
    this.modal = this.modalService.open(modal, this.modalOption);
    this.modal.result.then((result) => {
      console.log('sssssss');
      console.log(result);
      console.log('sssssss');
      this.cerrarModal.emit(true);
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      console.log('dddddddd');
      console.log(reason);
      this.cerrarModal.emit(true);
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  select2Changed(e: any, pos: number): void {
    this.modalData.formResponse[pos] = e.value;
  }

  getSelect2DefaultList(url: string) {
    /*this.resetService();
   this.tableService.webAddress.addUrl(url);
   let roles = undefined;
   this.tableService.get().subscribe(
     rolesList => {
       console.log(rolesList);
       roles = rolesList.data;
     },
     error => {
       console.log('ERROR');
       console.log(error);
       console.log('---------');
       roles = 'tazdingo';
     }
   );
  while (roles == null || roles === undefined) {
     console.log('No data yet');
   }/** */
    return [{
      id: 'Magellanic',
      text: 'Large Magellanic Cloud'
    },
    {
      id: 'Andromeda',
      text: 'Andromeda Galaxy'
    },
    {
      id: 'Sextans',
      text: 'Sextans A'
    }];
  };

  operation(data?: any) {
    this.resetOperation();
    this.setResponse();
    let operacion;
    switch (this.modalData.type) {
      case HttpOperations.POST:
        operacion = this.operationService.post(
          this.modalData.formResponseJSON
        );
        break;
      case HttpOperations.PATCH:
        operacion = this.operationService.patch(
          this.modalData.formResponseJSON
        );
        break;
      case HttpOperations.PUT:
        operacion = this.operationService.put(
          this.modalData.formResponseJSON
        );
        break;
      default:
        break;
    }
    operacion.subscribe(
      insertado => {
        console.log('Insertado exitosamente');
        this.modal.close();
        this.actualizarTabla.emit(true);
        this.modalData.formResponse = new Array<any>(6);
      },
      error => {
        this.modalData.formResponse = [];
        this.modal.close();
        console.log('Error');
      }
    );/** */
  }

  private setResponse() {
    let input = this.modalData.formInput;
    console.log('Tamanio');
    console.log(input.length);
    console.log('Form Response');
    console.log(this.modalData.formResponse);
    for (let index = 0; index < input.length; index++) {
      console.log('indice:' + index);

      console.log(this.modalData.formResponse[index]);

      this.modalData.formResponseJSON[input[index].column.name]
        = this.modalData.formResponse[index];

    }
    console.log('Modal Response');
    console.log(JSON.stringify(this.modalData.formResponseJSON));
  }

  private resetOperation() {
    this.operationService = new ImportacionesService(this.http);
    this.operationService.webAddress.addUrl(this.modalData.url);
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}
