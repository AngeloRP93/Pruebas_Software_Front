import {
  Component, OnInit, Input, Output,
  ViewChild, ElementRef, EventEmitter
} from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalData } from '../../interfaces/interfaces';
import { ImportacionesService } from '../../services/importacionesSyR.service';
import { Http } from '@angular/http';
import { Modal } from './modal';

@Component({
  selector: 'app-modal',
  templateUrl: 'modal.component.html'
})

export class ModalComponent extends Modal implements OnInit {
  @ViewChild('modal') modalRef: ElementRef;
  constructor(
    public http: Http,
    public modalService: NgbModal,
    public operationService: ImportacionesService) {
    super(http, modalService, operationService);

  }

  ngOnInit() {
    console.log(this.modalData);
    console.log(this.formAnterior);
    console.log(this.modalRef);
    let dom = jQuery('modal', ElementRef);
    console.log(dom);
    //Se guarda el anterior
    //console.log('Form Response:' + JSON.stringify(this.formResponse));

    this.open(this.modalRef, this.content);

  }

  cerrarModalButton() {
    this.cerrarModal.emit(true);
  }




}
