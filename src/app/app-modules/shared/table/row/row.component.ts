import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-row',
  templateUrl: 'row.component.html'
})

export class RowComponent implements OnInit {
  @Input() objeto: any;
  @Input() indice: string;
  elemento: any = '';
  constructor() { }

  ngOnInit() {
    let object = JSON.stringify(this.objeto);
    // console.log('Objeto:' + object);
    // console.log('Indice:' + JSON.stringify(this.indice));
    let index = this.indice['name'];
    // console.log(index);
    this.elemento = this.objeto[index];
    // if (isNaN(this.objeto[index]))
    //   this.elemento = this.objeto[index];
    // else{
    //   this.elemento = parseInt(this.objeto[index]);
    // }
    // console.log(this.elemento);

  }
}
