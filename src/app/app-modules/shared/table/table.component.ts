import { Component, OnInit, Input } from '@angular/core';
import { Table } from '../../super/table';
import { ConfigTable, FormElement, ModalData } from '../../interfaces/interfaces';
import { LoaderObject } from '../../super/objeto-loader';
import {
  NgbModal, ModalDismissReasons,
  NgbModalOptions, NgbModalRef
} from '@ng-bootstrap/ng-bootstrap';
import { ImportacionesService } from '../../services/importacionesSyR.service';
import { Http } from '@angular/http';
import { Modal } from '../modal/modal';
import { ModalComponent } from '../modal/modal.component';
declare let jQuery: any;

@Component({
  selector: 'app-table-dynamic',
  templateUrl: 'table.component.html',
  styleUrls: ['./table.component.scss',
    './validation.scss', './margins.scss'],
  providers: [ModalComponent]
})

export class DynamicTableComponent extends Table implements OnInit {
  editObject: any;
  mostrarEdit: boolean = false;
  mostrarAdd: boolean = false;
  marginLeft: string = '0px';
  closeResult: string;
  modalOption: NgbModalOptions = {};
  rows: Array<any> = [];
  select2Options: any = {
    theme: 'bootstrap'
  };
  @Input() titulo = '';
  @Input() url = '';
  @Input() modalEditData: ModalData;
  @Input() modalAddData: ModalData;
  @Input() addButton: any = {
    existe: true,
    desc: 'Agregar',
    img: 'fa fa-plus'
  };
  @Input() data: Array<any> = [
    {
      'name': 'Victoria Cantrell',
      'position': 'Integer Corporation',
      'office': 'Croatia',
      'ext': '0839',
      'startDate': '2015/08/19',
      'salary': 208.178
    }
  ];
  @Input() columns: Array<any> = [
    { title: 'Name', name: 'name' },
    { title: 'Position', name: 'position', sort: false },
    { title: 'Office', name: 'office', sort: 'asc' },
    { title: 'Extn.', name: 'ext', sort: '' },
    { title: 'Start date', name: 'startDate' },
    { title: 'Salary ($)', name: 'salary' }
  ];
  @Input() insert: string = 'register';
  @Input() buttons: Array<any> = [];
  @Input() maxSize: number = 5;
  @Input() numPages: number = 1;
  @Input() config: any = {
    paging: true,
    sorting: { columns: this.columns },
    filtering: { filterString: '', columnName: 'name' }
  };
  anchoTitulo: string;
  anchoTpx: number;
  private marginL: number = 906;
  constructor(
    public modalAdd: ModalComponent,
    public modalEdit: ModalComponent,
    private modalService: NgbModal,
    private tableService: ImportacionesService,
    private operationService: ImportacionesService,
    private http: Http) {
    super('');
    this.modalOption.backdrop = 'static';
    this.modalOption.keyboard = false;
  }

  ngOnInit() {
    console.log('Items Per Page:' + this.itemsPerPage);
    jQuery('.parsleyjs').parsley();
    let searchInput = jQuery('#table-search-input, #search-countries');
    searchInput
      .focus((e) => {
        jQuery(e.target).closest('.input-group').addClass('focus');
      })
      .focusout((e) => {
        jQuery(e.target).closest('.input-group').removeClass('focus');
      });
    console.log(this.modalAddData);
    this.modalAdd.setModalData(this.modalAddData);
    this.modalEdit.setModalData(this.modalEditData);
    this.getData(this.config);
  }

  select2Changed(e: any, pos: number): void {
    this.modalAdd.modalData.formResponse[pos] = e.value;
  }

  getSelect2DefaultList() {
    /*this.resetService();
   this.tableService.webAddress.addUrl('roles');
   let roles = undefined;
   this.tableService.get().subscribe(
     rolesList => {
       console.log(rolesList);
       roles = rolesList.data;
     },
     error => {
       console.log('ERROR');
       console.log(error);
       console.log('---------');
       roles = 'tazdingo';
     }
   );
  while (roles == null || roles === undefined) {
     console.log('No data yet');
   }/** */
    return [{
      id: 'Magellanic',
      text: 'Large Magellanic Cloud'
    },
    {
      id: 'Andromeda',
      text: 'Andromeda Galaxy'
    },
    {
      id: 'Sextans',
      text: 'Sextans A'
    }];
  };

  operationAdd() {
    let algo = this.modalAdd.operation();
    // console.log('Algo algo' + algo);
    setTimeout(() => {
      this.getData(this.config, { page: this.page, itemsPerPage: this.itemsPerPage });
    }, 1000);
  }

  operationEdit() {
    let algo = this.modalEdit.operation();
    // console.log('Algo algo' + algo);
    setTimeout(() => {
      this.getData(this.config, { page: this.page, itemsPerPage: this.itemsPerPage });
    }, 1000);
  }

  cerrarModal(any) {
    console.log('Algo');
    console.log(any);
    if (any) {
      this.mostrarAdd = false;
      this.mostrarEdit = false;
      $('.modal-backdrop').remove();
      $('.modal').remove();
      $('.modal-open').removeClass('modal-open');
      console.log(this.modalAddData);
    }
    console.log('++++++++++++++++++++');
  }

  actualizarTabla(actualizado) {
    if (actualizado) {
      this.getData(this.config, { page: this.page, itemsPerPage: this.itemsPerPage });
    }
  }

  modalAddShow() {
    this.mostrarAdd = true;
  }

  modalEditShow(object) {
    this.editObject = object;
    this.mostrarEdit = true;
  }

  private onChangeTable(
    config: any, page: any = { page: this.page, itemsPerPage: this.itemsPerPage }): any {
    this.getData(config, page);

  }

  private resetService() {
    this.tableService = new ImportacionesService(this.http);
    this.tableService.webAddress.addUrl(this.url);
  }
  private getData(config: any, page: any = { page: this.page, itemsPerPage: this.itemsPerPage }) {
    this.resetService();
    this.tableService.get().subscribe(
      productos => {
        this.data = productos.data.data;
        console.log(this.data);
        for (let index = 0; index < this.data.length; index++) {
          for (let index2 = 0; index2 < this.columns.length; index2++) {
            console.log(this.columns[index2]['type']);

            if (this.columns[index2]['type'] === 'number') {
              this.data[index][this.columns[index2]['name']] = parseInt(this.data[index][this.columns[index2]['name']]);
            }
            else if (this.columns[index2]['type'] === 'float') {
              this.data[index][this.columns[index2]['name']] = parseFloat(this.data[index][this.columns[index2]['name']]);
            }
          }
        }
        this.fillData(config, page);
        $('.text-nowrap').removeClass('text-nowrap');
      },
      error => {
        this.anchoTpx = 1160 - ($('.table-titulo').width() + $('.addButton').width());
        console.log('Ancho' + this.anchoTpx);
        this.anchoTitulo = this.anchoTpx + 'px';
        console.log('Error');
        console.log(error);
        console.log('ssssssssssssssssssssss');
        window.localStorage.setItem('actualizado', 'false');
        this.loading = false;
      },
      () => {
        this.anchoTpx = 1160 - ($('.table-titulo').width() + $('.addButton').width());
        console.log('Ancho' + this.anchoTpx);
        this.anchoTitulo = this.anchoTpx + 'px';
        this.loading = false;
      }
    );
  }
}
