import 'messenger/build/js/messenger.js';
import 'jquery-ui/ui/sortable.js';
import { Select2Module } from 'ng2-select2';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgModule, NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AlertModule, TooltipModule } from 'ng2-bootstrap';
import { ButtonsModule, DropdownModule, PaginationModule } from 'ng2-bootstrap';
import { Ng2TableModule } from 'ng2-table';
import { DataTableModule } from 'angular2-datatable';
import { ModalModule } from 'angular2-modal';
import { ImportacionesService } from '../services/importacionesSyR.service';
import { DynamicTableComponent } from './table/table.component';
import { RowComponent } from './table/row/row.component';
import { WidgetModule } from '../../layout/widget/widget.module';
import { ModalComponent } from './modal/modal.component';
import 'parsleyjs';
import { SearchPipe } from './table/search/search.pipe';
import 'bootstrap-colorpicker';
import 'bootstrap-markdown/js/bootstrap-markdown.js';
import 'bootstrap-select/dist/js/bootstrap-select.js';
import 'parsleyjs';
declare let global: any;
// libs
/* tslint:disable */
let markdown = require('markdown').markdown;
/* tslint:enable */
global.markdown = markdown;

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    Ng2TableModule,
    WidgetModule,
    Select2Module,
    ModalModule.forRoot(),
    DataTableModule,
    AlertModule.forRoot(),
    TooltipModule.forRoot(),
    ButtonsModule.forRoot(),
    DropdownModule.forRoot(),
    PaginationModule.forRoot(),
    NgbModule.forRoot(),
  ],
  schemas: [NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA],
  exports: [
    CommonModule,
    DynamicTableComponent
  ],
  declarations: [
    RowComponent,
    DynamicTableComponent,
    ModalComponent,
    SearchPipe
  ],
  providers: [ImportacionesService]
})
export class SharedModule { }
