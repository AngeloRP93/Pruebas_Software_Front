import { Component, EventEmitter, OnInit, ElementRef, Output } from '@angular/core';
import { AppConfig } from '../../app.config';
import { Router } from '@angular/router';
declare let jQuery: any;

@Component({
  selector: '[navbar]',
  templateUrl: './navbar.template.html',
  styleUrls: ['./navbar.component.scss']
})
export class Navbar implements OnInit {
  @Output() toggleSidebarEvent: EventEmitter<any> = new EventEmitter();
  @Output() toggleChatEvent: EventEmitter<any> = new EventEmitter();
  userName: string = '';
  $el: any;
  marginRight: string = '0px';
  marginLeft: string = '0px';
  config: any;
  private marginL: number = 1025;

  constructor(
    el: ElementRef,
    config: AppConfig,
    private router: Router) {
    this.$el = jQuery(el.nativeElement);
    this.config = config.getConfig();
    this.userName = window.localStorage.getItem('userName');
  }

  toggleSidebar(state): void {
    console.log('Estado:' + state);
    this.changeMarginRight();
    this.toggleSidebarEvent.emit(state);
  }

  toggleChat(): void {
    this.toggleChatEvent.emit(null);
  }

  ngAfterViewChecked() {
    //Called after every check of the component's view. Applies to components only.
    //Add 'implements AfterViewChecked' to the class.
    this.ajustarPantalla();
  }

  ngOnInit(): void {
    this.ajustarPantalla();
    /*setTimeout(() => {
      let $chatNotification = jQuery('#chat-notification');
      $chatNotification.removeClass('hide').addClass('animated fadeIn')
        .one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', () => {
          $chatNotification.removeClass('animated fadeIn');
          setTimeout(() => {
            $chatNotification.addClass('animated fadeOut')
              .one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd' +
                ' oanimationend animationend', () => {
                $chatNotification.addClass('hide');
              });
          }, 8000);
        });
      $chatNotification.siblings('#toggle-chat')
        .append('<i class="chat-notification-sing animated bounceIn"></i>');
    }, 4000);/** */

    /*this.$el.find('.input-group-addon + .form-control').on('blur focus', function(e): void {
      jQuery(this).parents('.input-group')
        [e.type === 'focus' ? 'addClass' : 'removeClass']('focus');
    });/** */
  }

  logOut() {
    window.localStorage.removeItem('userName');
    this.router.navigate(['/login']);
  }

  private changeMarginRight() {
    if (this.marginRight == '203px') {
      this.marginRight = '0px'
    } else {
      this.marginRight = '203px'
    }
  }

  private ajustarPantalla() {
    let screenPx = window.innerWidth;
    //console.log('Screen Px:' + screenPx);
    this.marginL = 1025 - (1517 - screenPx);
    if (screenPx <= 766)
      this.marginL -= 30;
    this.marginLeft = this.marginL + 'px';
  }
}
