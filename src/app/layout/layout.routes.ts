import { Routes, RouterModule }  from '@angular/router';
import { Layout } from './layout.component';
// noinspection TypeScriptValidateTypes
const routes: Routes = [
  { path: '', component: Layout, children: [
    { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
    { path: 'dashboard', loadChildren: '../dashboard/dashboard.module#DashboardModule' },
    { path: 'productos', loadChildren: '../app-modules/maintenances/productos/productos.module#ProductosModule' },
    { path: 'users', loadChildren: '../app-modules/maintenances/administrador/administrador.module#AdministradorModule' },
    { path: 'another-page', loadChildren: '../another/another.module#AnotherModule' },
    { path: 'prueba', loadChildren: '../app-modules/principal.module#PrincipalModule' }
  ]}
];

export const ROUTES = RouterModule.forChild(routes);
